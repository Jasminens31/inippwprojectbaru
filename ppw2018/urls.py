from django.contrib import admin
from django.urls import path, include
import story8.urls as story8
import story9.urls as story9
import story10.urls as story10
import story11.urls as story11

urlpatterns = [
    path('admin/', admin.site.urls),
    path('story8/',include('story8.urls')),
    path('',include('story9.urls')),
    path('story10/',include('story10.urls')),
    path('daftar/',include('django.contrib.auth.urls')),
    path('',include('story11.urls')),
    path('auth/',include('social_django.urls', namespace='social')),
]
