$(document).ready(function () {
	$.ajax({
		method : "GET",
		url : "https://www.googleapis.com/books/v1/volumes?q=quilting",
		success: function(hasil) {
			var jsonBooks = JSON.stringify(hasil);
			var books = JSON.parse(jsonBooks);
			var booksArray = books.items;
			for (var a = 0; a < booksArray.length; a++) {
				var title = booksArray[a].volumeInfo.title;
				var author = title.replace(/'/g, "\\'");
				var button  = '<button class = "bttn"' +  'onClick="addFavorite(\'' + author + '\')">' + '<i class="ic"></i><button>';
				var html = '<tr class="table-info">' + '<td>' + button + title + '</td>' + '<td>' + booksArray[a].volumeInfo.authors + '</td>' +
							'<td>' + booksArray[a].volumeInfo.publishedDate + '</td>' + '</tr>';
				$('tbody').append(html); 
			}
		},
		error: function(error) {
			alert("Buku tidak ditemukan bung")
		}
	})
});

var addFavorite= function(title) {
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	$.ajax({
		method: "POST",
		url:"/",
		headers:{
			"X-CSRFToken": csrftoken
	},
		data: {title:title},
		success:function(hasil) {
			button = '<button class="button-delete"' + 'onClick="deleteFavorite(\'' + hasil.id + '\')">' + '<i class="del"></i></button>';
			html = "<div id='" + hasil.id + "'>" + hasil.hasil + button + "</div>";
			$(".data").append(html);
			$(".count").replaceWith("<span class='count'>" + hasil.count + "</span>")
		},
		error:function(error){
			alert("cannot get data from server")
		}

	});

};

var deleteFavorite = function(id) {
	var csrftoken = $("[name=csrfmiddlewaretoken]").val();
	$.ajax({
		method:"POST",
		url: "/delete",
		headers:{
			"X-CSRFToken": csrftoken
	},
		data: {id:id},
		success: function(count) {
			$(".count").replaceWith("<span class='count'>" + count.count + "</span>")
			$("#"+id).remove();
		}

	});
};
