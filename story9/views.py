from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
import requests
from .models import Favorite

# Create your views here.
# def story9(request):
# 	return render(request, "library.html")

response = {}

def story9(request):
    return render(request, 'library.html', response)

def datas(request):
	print('method post')
	mysearch = request.GET.get('search')
	URL = "https://www.googleapis.com/books/v1/volumes?q=" + mysearch
	get_json = requests.get(URL).json()
	return JsonResponse(get_json)
	

    # url = "https://www.googleapis.com/books/v1/volumes?q="
    # dts = requests.get(url).json()
    # return JsonResponse(dts)
