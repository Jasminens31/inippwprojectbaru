from django.db import models

# Create your models here.


class Favorite(models.Model):
    title = models.CharField(max_length=500)
    id = models.CharField(max_length=500, primary_key=True, unique=True)
