var counter = 0;
$(document).ready(function(){
    var search = ""
    $("input").change(function(){
        search = $(this).val();

    });
    
    $("button").click(function() {
            // $("table").clear();
            $("#shelf").html("")
            $.ajax({
                url: "/data",
                data: {'search':search},
                success: function(result){
                    result = result.items;
                    console.log(result);
                    
                    var hdr = "<thead><tr><th>Title</th><th>Author</th><th>Thumbnail</th><th>Description</th><th></th></tr></thead>";
                    $("#shelf").append("<tbody>");
                    $("#shelf").append(hdr);
                    //dataLength = result.length;
                    for(i=0; i<result.length; i++){
                        var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + 
                        result[i].volumeInfo.authors + "</td><td>"+
                         "<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>"+"</td><td>" + 
                          result[i].volumeInfo.description +"</td><td>"+
                           "<button class='button' style = 'background-color: Transparent; border: none'><i class='fa fa-star' id='star' style = 'color : gray'></i></button>"+"</td></tr>";
                           $("#shelf").append(tmp);
                       }
                       $("#shelf").append("</tbody>");
                   }
               });
        });

    $(document).on('click', '#star', function() {
        if( $(this).hasClass('clicked') ) {
            counter -=1;
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counter = counter+ 1;
        }
        $('#statusBuku').html(counter);

    });
});
