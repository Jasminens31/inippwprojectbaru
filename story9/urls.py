from django.urls import path
from django.contrib import admin
from . import views
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from .views import *


urlpatterns = [
    # path('delete', views.delete, name="delete"),
    path('story9', views.story9, name='story9'),
    url(r'data', datas, name = 'datas'),
    # path('getdata', views.getData, name='getdata')

]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()