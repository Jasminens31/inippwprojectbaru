from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
# from .models import Favorite
# Create your tests here.


class libraryUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/story9')
        self.assertEqual(response.status_code,200)

    def test_url_using_library_template(self):
        response = Client().get('/story9')
        self.assertTemplateUsed(response, 'library.html')

    def test_urls_using_display_library_func(self):
        found = resolve('/story9')
        self.assertEqual(found.func, story9)
