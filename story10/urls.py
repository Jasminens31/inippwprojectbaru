from django.urls import path
from .views import *


urlpatterns = [
	path('', formReg, name="form"),
	path('addSubscribe', addSub, name="addSubscribe"),
	path('validasi', validasi, name="validasi"),
	path('unSubscribe', delete, name="unSubscribe"),

]
