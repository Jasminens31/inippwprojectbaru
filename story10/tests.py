from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class lab10test(TestCase):
	def test_url_profile_is_exist(self):
		response = Client().get('/story10/')
		self.assertEqual(response.status_code,200)

	# def test_profile_using_profile_template(self):
	# 	response = Client().get('')
	# 	self.assertTemplateUsed(response,'mysubscribe.html')

	def test_using_profile_func(self):
		found = resolve('/story10/')
		self.assertEqual(found.func,formReg)

	def test_model_can_add_subscriber(self):
		add_subscriber = Subscribe.objects.create(nama = "john",email = "john@yahoo.com", password = "johnjohn")
		count_obj = Subscribe.objects.all().count()
		self.assertEqual(count_obj,1)

	# def test_add_subscriber(self):
	# 	response = Client().post('/addSubscribe/',{'nama':"nama",'email':"email",'password':"password"})
	# 	self.assertEqual(response.status_code,200)

