from django import forms

class subscriber(forms.Form):
    name_attrs = {
        'type': 'email',
        'id' : 'text1',
        'placeholder':'Input Name',
        'class' : "subscribe-input",
        'class': 'todo-form-input',
        'required':True,
    }
    email_attrs = {
        'type': 'email',
        'id' : 'text2',
        'placeholder':'Email',
        'class' : "subscribe-input",
        'class': 'todo-form-input',
        'required':True,
    }
    email1_attrs = {
        'type': 'email',
        'id' : 'text4',
        'placeholder':'Email',
        'class' : "subscribe-input",
        'class': 'todo-form-input',
        'required':True,
    }
    pass_attrs = {
        'type': 'password',
        'id' : 'text3',
        'placeholder':'Password',
        'class' : "subscribe-input",
        'class': 'todo-form-input',
        'required':True,
    }
    nama = forms.CharField(widget=forms.TextInput(attrs=name_attrs), max_length=50)
    email = forms.EmailField(widget=forms.EmailInput(attrs=email_attrs))
    email1 = forms.EmailField(widget=forms.EmailInput(attrs=email1_attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=pass_attrs), min_length=8)
